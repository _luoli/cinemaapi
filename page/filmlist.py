from common.Driver import Driver
from common.GetElement import Get_Element


class FilmList:
    def __init__(self):
        self.driver = Driver.get_driver()
        self.element = Get_Element()

    def filmlist_implement1(self):
        return self.element.get_element('xpath', '/html/body/header/nav/ul/li[4]/a')

    def filmlist_implement2(self):
        return self.driver.find_element_by_xpath('//*[@id="MovieTop"]/div[2]/div/dl/dd[1]/div[3]/div[1]/a')

    def filmlist_1(self):
        return self.element.get_element('xpath','//*[@id="MovieTop"]/div[2]/div/div/p[2]')

    def filmlist_2(self):
        return self.element.get_element('xpath','//*[@id="MovieTop"]/div[2]/div/dl/dd[1]/div[3]/div[1]/a')

    def filmlist_3(self):
        return self.element.get_element('xpath','//*[@id="moviedetail"]/div[2]/div[2]/div[3]/a')