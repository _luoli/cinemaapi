# createtime:2021/3/24 14:39
# user:luoli
# project:pycharmprojects

from cinema_test_api.common.Session import Session


class DoMovieQuery():
    '''
    查询电影信息
    '''
    def __init__(self):
        self.session = Session.get_session()

    def test_case1(self, host, movieName):
        resp = self.session.get(f"{host}/AllMovie/MovieByKeyWord", params=movieName)
        return resp


if __name__ == '__main__':
    domq = DoMovieQuery()
    print(domq.test_case1("http://192.168.0.111:8080", "活着"))
