# createtime:2021/3/24 16:13
# user:luoli
# project:pycharmprojects
from cinema_test_api.common.string_to_dict import str_to_dict
from cinema_test_api.common.Session import Session


class DoCinemaQuery():
    '''
    查询影院信息
    '''

    def __init__(self):
        self.session = Session.get_session()

    def test_case1(self, host, provice, city, brand, hallType, area, page, ):
        data = str_to_dict(f"page={page}&province={provice}&city={city}&brand={brand}&hallType={hallType}&area={area}")
        resp = self.session.get(f"{host}/cinema/getAllCinemaByAll", params=data)
        # resp.encoding('utf-8')
        print(resp.url)
        return resp


if __name__ == '__main__':
    domq = DoCinemaQuery()
    print(domq.test_case1(host="http://192.168.0.111:8080", city="杭州市").text)
