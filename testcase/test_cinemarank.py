import os
import time

from action.do_cinemarank import DoCinemaRank
from common.GetmathodPath import get_mathodPath
from common.ReadFile import ReadFile
from common.ReadDb import ReadDb
import unittest
from ddt import ddt,unpack,data

from common.read_csv import readcsv

@ddt()
class Test_CinemaRank(unittest.TestCase):

    path = os.path.dirname(os.path.dirname(__file__)) + "/testdata/cinemarank.csv"

    @classmethod
    def setUpClass(cls) -> None:
        cls.add = DoCinemaRank()
        cls.db = ReadDb()


    @data(*readcsv(path))  # *是解包
    @unpack
    def test_crank_case1(self,caseid,title,status,expect):
        get_mathodPath(self, caseid, title)
        resp = self.add.CinemaRank(caseid,title,status,expect)
        self.assertEqual(resp.text,'success',msg='测试通过')


    #
    # # @ReadFile('../testdata/cinemarank.csv')
    # def test_crank_case1(self,caseid,title,status,expect):
    #     resp = self.cine.CinemaRank(status)
    #     test_time = time.strftime('%Y-%m-%d %H:%M:%S')
    #     test_type = "api测试"
    #     test_module = '电影排名'
    #     if resp.text == expect:
    #         print(f'{caseid}--{title}-测试通过')
    #         sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
    #               f'values ("{test_type}","{test_module}","{caseid}","{title}","success","无","{test_time}"' \
    #               f');'
    #         self.db.insert(sql)
    #     else:
    #         print(f'{caseid}--{title}-测试失败')
    #         sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
    #               f'values ("{test_type}","{test_module}","{caseid}","{title}","failed","无","{test_time}"' \
    #               f');'
    #         self.db.insert(sql)


if __name__ == '__main__':
    rank=Test_CinemaRank()
    # rank.test_crank_case1()
